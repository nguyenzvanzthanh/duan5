import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import posts from "./routers/posts.js";
import dbConfig from "./config/db.config.js";
const app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json({limit:'30mb'}));
app.use(bodyParser.urlencoded({extended:true,limit:'30mb'}));
app.use(cors());

app.use('/posts',posts);



mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    app.listen(PORT, () =>{
        console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });
